FROM alpine:3.18

# Env
ENV \
MY_USER=ansible \
MY_GROUP=ansible \
MY_UID=1000 \
MY_GID=1000

# Add user and group
RUN set -eux \
&& addgroup -g ${MY_GID} ${MY_GROUP} \
&& adduser -h /home/ansible -s /bin/bash -G ${MY_GROUP} -D -u ${MY_UID} ${MY_USER}

# Install Ansible package
RUN apk --update --no-cache add \
sudo ca-certificates git openssh-client openssl python3 py3-pip py3-cryptography rsync sshpass && \
apk --no-cache add --virtual build-dependencies \
python3-dev libffi-dev openssl-dev build-base && \
pip3 install --upgrade wheel pip cffi && \
pip3 install ansible-core && \
pip3 install ansible-lint && \
mkdir /ansible && chmod 640 /ansible && \
apk del build-dependencies && \
rm -rf /var/cache/apk/*

RUN mkdir -p /etc/ansible && \
cat /etc/passwd && \
echo 'localhost' > /etc/ansible/hosts && \
echo -e """\
\n\
Host *\n\
StrictHostKeyChecking no\n\
UserKnownHostsFile=/dev/null\n\
HostKeyAlgorithms=ssh-rsa\n\
PubkeyAcceptedKeyTypes=ssh-ed25519,ssh-rsa,rsa-sha2-256,rsa-sha2-512\n\
""" >> /etc/ssh/ssh_config

WORKDIR /ansible

#USER ansible

# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]
